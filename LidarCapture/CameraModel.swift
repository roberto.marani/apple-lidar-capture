//
//  CameraModel.swift
//  LidarCapture
//
//  Created by Roberto Marani on 16/01/23.
//

import Foundation
import SwiftUI
import AVFoundation
import SceneKit

class CameraModel: NSObject,ObservableObject,AVCapturePhotoCaptureDelegate {
    @Published var isTaken = false
    @Published var session = AVCaptureSession()
    @Published var alert = false
    @Published var photoOutput = AVCapturePhotoOutput()
    @Published var depthDataOutput = AVCaptureDepthDataOutput()
    @Published var preview : AVCaptureVideoPreviewLayer!
    @Published var cameraCalibrationData : AVCameraCalibrationData!
    @Published var isSaved = false
    @Published var picData = UIImage()
    @Published var depthMapData = Data(count: 0)
    @Published var rawDepthMapData : CVPixelBuffer!
    lazy var context = CIContext()
    
    @Published var deviceOrientation: UIDeviceOrientation!
    
    enum ConfigurationError: Error {
        case lidarDeviceUnavailable
        case requiredFormatUnavailable
    }
    
    func Check(){
            // check permission
            switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized:
                setUp()
                return
                // Setting Up Session
            case .notDetermined:
                // retusting for permission....
                AVCaptureDevice.requestAccess(for: .video) { (status) in
                    if status{
                        self.setUp()
                    }
                }
            case .denied:
                self.alert.toggle()
                return
            default:
                return
            }
        }
        
    func setUp(){
        do{
            self.session.beginConfiguration()
                    
            // VERIFY WHICH CAMERA YOU NEED
            //let device = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
            //let input = try AVCaptureDeviceInput(device: device!)
            
            //let device = AVCaptureDevice.default(.builtInLiDARDepthCamera, for: .video, position: .back)
            
            guard let device = AVCaptureDevice.default(.builtInLiDARDepthCamera, for: .video, position: .back) else {
                throw ConfigurationError.lidarDeviceUnavailable
            }
            
            // Find a match that outputs video data in the format the app's custom Metal views require.
            guard let format = (device.formats.last { format in
                //format.formatDescription.dimensions.width == 1920 &&
                format.formatDescription.mediaSubType.rawValue == kCVPixelFormatType_420YpCbCr8BiPlanarFullRange &&
                !format.isVideoBinned &&
                !format.supportedDepthDataFormats.isEmpty
            }) else {
                throw ConfigurationError.requiredFormatUnavailable
            }
            
            // Find a match that outputs depth data in the format the app's custom Metal views require.
            guard let depthFormat = (format.supportedDepthDataFormats.last { depthFormat in
                depthFormat.formatDescription.mediaSubType.rawValue == kCVPixelFormatType_DepthFloat16
            }) else {
                throw ConfigurationError.requiredFormatUnavailable
            }
            
            // Begin the device configuration.
            try device.lockForConfiguration()

            // Configure the device and depth formats.
            device.activeFormat = format
            device.activeDepthDataFormat = depthFormat

            // Finish the device configuration.
            device.unlockForConfiguration()
            
            let input = try AVCaptureDeviceInput(device: device)
            if self.session.canAddInput(input){
                self.session.addInput(input)
            }
            
            if self.session.canAddOutput(self.photoOutput){
                self.photoOutput.maxPhotoQualityPrioritization = .quality
                self.session.addOutput(self.photoOutput)
            }
            // Enable delivery of depth data after adding the output to the capture session.
            self.photoOutput.isDepthDataDeliveryEnabled = true
            
            if self.session.canAddOutput(self.depthDataOutput){
                self.depthDataOutput.isFilteringEnabled = true
                self.session.addOutput(self.depthDataOutput)
            }
            
            self.session.commitConfiguration()
            
        }
        catch{
            print(error.localizedDescription)
        }
    }
    
    func takePic(){
        DispatchQueue.global(qos: .background).async {
            
            var photoSettings: AVCapturePhotoSettings
            if  self.photoOutput.availablePhotoPixelFormatTypes.contains(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange) {
                photoSettings = AVCapturePhotoSettings(format: [
                    kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_420YpCbCr8BiPlanarFullRange
                ])
            } else {
                photoSettings = AVCapturePhotoSettings()
            }
            
            // Capture depth data with this photo capture.
            photoSettings.isDepthDataDeliveryEnabled = true
            self.photoOutput.capturePhoto(with: photoSettings, delegate: self)
            
            //self.photoOutput.capturePhoto(with: AVCapturePhotoSettings(), delegate: self)
            // TO BE CHECKED
            DispatchQueue.main.async { Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false) { (timer) in self.session.stopRunning() } }
            //self.session.stopRunning()
            // OR (to be fixed)
            // DispatchQueue.main.asyncAfter(deadline: .now() + 0.5 {self.session.stopRunning()})
            //------------------------
            
            DispatchQueue.main.async {
                    withAnimation{self.isTaken.toggle()}
            }
        }
    }
        
    func reTake(){
        DispatchQueue.global(qos: .background).async {
            self.session.startRunning()
            DispatchQueue.main.async {
                withAnimation{self.isTaken.toggle()}
                self.isSaved = false
            }
        }
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if error != nil{
            return
        }
        //guard let imageData = photo.fileDataRepresentation() else{return}
        //self.picData = imageData
        
        deviceOrientation = UIDevice.current.orientation
        
        guard let img = photo.cgImageRepresentation() else { return }
        let temp = CIImage(cgImage: img)
        var ciImage = temp;
        
        switch deviceOrientation {
        case .portrait:
            ciImage = temp.oriented(forExifOrientation: 6)
        case .portraitUpsideDown:
            ciImage = temp.oriented(forExifOrientation: 8)
        case .landscapeLeft:
            ciImage = temp.oriented(forExifOrientation: 1)
        case .landscapeRight:
            ciImage = temp.oriented(forExifOrientation: 3)
        default:
            ciImage = temp.oriented(forExifOrientation: 3)
        }

        guard let cgImage = CIContext(options: nil).createCGImage(ciImage, from: ciImage.extent) else { return }
        self.picData = UIImage(cgImage: cgImage)
        
        /*// Retrieve the image and depth data.
        guard let pixelBuffer = photo.pixelBuffer,
              let depthData = photo.depthData,
              let cameraCalibrationData = depthData.cameraCalibrationData else { return }
        // Convert the depth data to the expected format.
        let convertedDepth = depthData.converting(toDepthDataType: kCVPixelFormatType_DepthFloat16)
        */

        // Cache the depth data, if it exists, as a disparity map.
        if let depthData = photo.depthData?.converting(toDepthDataType: kCVPixelFormatType_DisparityFloat32), let colorSpace = CGColorSpace(name: CGColorSpace.linearGray) {
            let depthImage = CIImage( cvImageBuffer: depthData.depthDataMap, options: [ .auxiliaryDisparity: true ] )
            self.depthMapData = context.tiffRepresentation(of: depthImage,
                                                  format: .Lf,
                                                  colorSpace: colorSpace,
                                                  options: [.disparityImage: depthImage])!
        }
        
        // Cache the depth data, if it exists in meters.
        if let depthData = photo.depthData?.converting(toDepthDataType: kCVPixelFormatType_DepthFloat32) {
            // Create the raw data
            self.rawDepthMapData = depthData.depthDataMap //AVDepthData -> CVPixelBuffer
            self.cameraCalibrationData = depthData.cameraCalibrationData
        }
        
        
        //Save Intrinsic Camera Parameters
        let dirFolder = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        guard let jsonURL = dirFolder?.appendingPathComponent("Camera Intrinsics").appendingPathExtension("json") else {
            fatalError("Not able to create URL")
        }
        let filePath = jsonURL.path
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: filePath) {
            saveIntrinsics(jsonPathWithFileName: jsonURL)
        }
    }

    func savePic(){
        
        let timeStr = getTimeString()
        
        // Find the document folder
        let dirFolder = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        // -------------------------------------------------------------------------------
        // Write RGB
        //let image = UIImage(data: self.picData)!
        let image = self.picData
        // Create URL for RGB image
        guard let ColorURL = dirFolder?.appendingPathComponent(timeStr + "_Color").appendingPathExtension("png") else {
            fatalError("Not able to create URL")
        }
        if let data = image.pngData() {
            do {
                try data.write(to: ColorURL)
            } catch {
                assertionFailure("Failed writing to URL: \(ColorURL), Error: " + error.localizedDescription)
            }
        }
        // UIImageWriteToSavedPhotosAlbum(image,nil,nil,nil)
        
        
        // -------------------------------------------------------------------------------
        // Write Depth as png image
        let depth = UIImage(data: self.depthMapData)!
        
        //Check orientation
        var rotatedDepth: UIImage!
        switch deviceOrientation {
        case .portrait:
            rotatedDepth = depth.rotate(radians: .pi/2) //.fixedOrientation().imageRotatedByDegrees(degrees: 90.0)
        case .portraitUpsideDown:
            rotatedDepth = depth.rotate(radians: -.pi/2) //.fixedOrientation().imageRotatedByDegrees(degrees: -90.0)
        case .landscapeLeft:
            rotatedDepth = depth
        case .landscapeRight:
            rotatedDepth = depth.rotate(radians: .pi) //.fixedOrientation().imageRotatedByDegrees(degrees: 180.0)
        default:
            rotatedDepth = depth.rotate(radians: .pi) //.fixedOrientation().imageRotatedByDegrees(degrees: 180.0)
        }
        
        // Create URL for depth image
        guard let DepthURL = dirFolder?.appendingPathComponent(timeStr + "_Depth").appendingPathExtension("png") else {
            fatalError("Not able to create URL")
        }
        if let data = rotatedDepth.pngData() {
            do {
                try data.write(to: DepthURL)
            } catch {
                assertionFailure("Failed writing to URL: \(DepthURL), Error: " + error.localizedDescription)
            }
        }
        // UIImageWriteToSavedPhotosAlbum(rotatedDepth,nil,nil,nil)
        
        
        // -------------------------------------------------------------------------------
        // Write Raw Depth as txt file
        // Useful data
        let depthW = CVPixelBufferGetWidth(self.rawDepthMapData) // ipad pro 11' 2022 = 768
        let depthH = CVPixelBufferGetHeight(self.rawDepthMapData) // ipad pro 11' 2022 = 432
        CVPixelBufferLockBaseAddress(self.rawDepthMapData, CVPixelBufferLockFlags(rawValue: 0))
        
        // Create URL for depth image
        guard let DepthRawURL = dirFolder?.appendingPathComponent(timeStr + "_Depth").appendingPathExtension("bin") else {
            fatalError("Not able to create URL")
        }
        
        // Convert the base address to a safe pointer of the appropriate type
        let depthFloatBuffer = unsafeBitCast(CVPixelBufferGetBaseAddress(self.rawDepthMapData), to: UnsafeMutablePointer<Float32>.self)

        var depthTempBuffer = [Float32]()
               
        switch deviceOrientation {
        case .portrait:
            for u in 0...(depthW - 1) {
                for v in (0...(depthH - 1)).reversed() {
                    depthTempBuffer.append(depthFloatBuffer[u + v*depthW])
                }
            }
        case .portraitUpsideDown:
            for u in (0...(depthW - 1)).reversed() {
                for v in 0...(depthH - 1) {
                    depthTempBuffer.append(depthFloatBuffer[u + v*depthW])
                }
            }
        case .landscapeLeft:
            for i in 0...(depthH * depthW - 1) {
                depthTempBuffer.append(depthFloatBuffer[i])
            }
        case .landscapeRight:
            for i in (0...(depthH * depthW - 1)).reversed() {
                depthTempBuffer.append(depthFloatBuffer[i])
            }
        default:
            for i in (0...(depthH * depthW - 1)).reversed() {
                depthTempBuffer.append(depthFloatBuffer[i])
            }
        }
        
        
        let depthFloatData = Data(bytes: &depthTempBuffer, count: depthTempBuffer.count * MemoryLayout<Float32>.stride)
        do {
            try depthFloatData.write(to: DepthRawURL)
        } catch {
            assertionFailure("Failed writing to URL: \(DepthRawURL), Error: " + error.localizedDescription)
        }
        
        //let points = createPointCloud(depthValues: depthTempBuffer)
                
        self.isSaved = true
        
        self.reTake()
    }
    
    func getTimeString() -> String {
        let currentDate = Date().timeIntervalSince1970
                
        let date = Date(timeIntervalSince1970: currentDate)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyyMMddHHmmss" //Specify your format that you want
        let timeStr = dateFormatter.string(from: date)
        return timeStr
    }
    
    func saveIntrinsics(jsonPathWithFileName: URL) {

        let ratio = Float(self.cameraCalibrationData.intrinsicMatrixReferenceDimensions.width) / Float(CVPixelBufferGetWidth(self.rawDepthMapData))
        let fx = self.cameraCalibrationData.intrinsicMatrix.columns.0[0] / ratio
        let fy = self.cameraCalibrationData.intrinsicMatrix.columns.1[1] / ratio
        let cx = self.cameraCalibrationData.intrinsicMatrix.columns.2[0] / ratio
        let cy = self.cameraCalibrationData.intrinsicMatrix.columns.2[1] / ratio
        
        let fxStr = String(format: "\"fx\":%.6f", fx)
        let fyStr = String(format: "\"fy\":%.6f", fy)
        let cxStr = String(format: "\"cx\":%.6f", cx)
        let cyStr = String(format: "\"cy\":%.6f", cy)
        
        let jsonString = "{\n\t" + fxStr + ",\n\t"  + fyStr + ",\n\t" + cxStr + ",\n\t" + cyStr + "\n}"
        if let jsonData = jsonString.data(using: .utf8) {
            do {
                try jsonData.write(to: jsonPathWithFileName)
            } catch {
                assertionFailure("Failed writing to URL: \(jsonPathWithFileName), Error: " + error.localizedDescription)
            }
        }
    }
    
    func createPointCloud(depthValues: [Float32]) -> [SCNVector3] {
        
        let depthW  = CVPixelBufferGetWidth(self.rawDepthMapData)
        var intrinsics = self.cameraCalibrationData.intrinsicMatrix
        let referenceDimensions = self.cameraCalibrationData.intrinsicMatrixReferenceDimensions
        
        let ratio = Float(referenceDimensions.width) / Float(depthW)
        intrinsics.columns.0[0] /= ratio
        intrinsics.columns.1[1] /= ratio
        intrinsics.columns.2[0] /= ratio
        intrinsics.columns.2[1] /= ratio
        
        return depthValues.enumerated().map {
            let z = Float($0.element)
            let index = $0.offset
            let u = Float(index % depthW)
            let v = Float(index / depthW)
            
            let x = (u - intrinsics.columns.2[0]) * z / intrinsics.columns.0[0];
            let y = (v - intrinsics.columns.2[1]) * z / intrinsics.columns.1[1];
            
            return SCNVector3(x, y, z)
        }
    }
}
   

