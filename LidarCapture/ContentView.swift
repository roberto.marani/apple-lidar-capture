//
//  ContentView.swift
//  LidarCapture
//
//  Created by Roberto Marani on 16/01/23.
//

import SwiftUI
import AVFoundation

struct ContentView: View {
    var body: some View {
        CameraView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct CameraView: View {
    
    @StateObject var camera = CameraModel()
    
    var body: some View {
        ZStack {
            
            CameraPreview(camera: camera)
                .ignoresSafeArea(.all, edges: .all)
                .background(Color.black)
                
            HStack {
                Spacer()
                
                VStack() {
                    if camera.isTaken {
                        
                        Button(action: camera.reTake, label: {
                            ZStack{
                                RoundedRectangle(cornerRadius: 10.0,style: .circular)
                                    .fill(Color.red)
                                    .frame(width: 75, height: 75)
                                Image(systemName: "trash")
                                    .resizable()
                                    .aspectRatio(1, contentMode: .fill)
                                    .foregroundColor(Color.white)
                                    .background(Color.red)
                                    .frame(width: 40, height: 40)
                            }
                        })
                        .padding(.trailing, 27)
                        
                        Spacer()

                        Button(action: {if !camera.isSaved{camera.savePic()}}, label: {
                            ZStack{
                                RoundedRectangle(cornerRadius: 10.0,style: .circular)
                                    .fill(Color.green)
                                    .frame(width: 75, height: 75)
                                Image(systemName: "checkmark.circle")
                                    .resizable()
                                    .aspectRatio(1, contentMode: .fill)
                                    .foregroundColor(Color.white)
                                    .background(Color.green)
                                    .frame(width: 40, height: 40)
                            }
                        })
                        .padding(.trailing, 27)
                        
                    }
                    else {
                        
                        Button(action: camera.takePic, label:{
                                ZStack {
                                    Circle()
                                        .fill(Color.orange)
                                        .frame(width: 75, height: 75)
                                    Circle()
                                        .stroke(Color.white, lineWidth: 2)
                                        .frame(width: 89, height: 89)
                                }
                        })
                        .padding(.trailing, 20)
                        
                    }
                }
                .frame(height: 250)
            }
        }
        .onAppear(perform: {
            camera.Check()
        })
    }
    
}


struct CameraPreview: UIViewRepresentable{
    
    @ObservedObject var camera : CameraModel
        
    func makeUIView(context: Context) -> some UIView {
        let view = UIView(frame: UIScreen.main.bounds)
        
        camera.preview = AVCaptureVideoPreviewLayer(session: camera.session)
        camera.preview.frame = view.frame
        camera.preview.connection?.videoOrientation = AVCaptureVideoOrientation.landscapeRight
        //camera.preview.videoGravity = .resizeAspect
        view.layer.addSublayer(camera.preview)
        camera.session.startRunning()

        return view
    }
    
    func updateUIView(_ uiView: UIViewType, context: Context) {
        camera.preview.connection?.videoOrientation = AVCaptureVideoOrientation.landscapeRight
    }
    
}



