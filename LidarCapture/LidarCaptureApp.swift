//
//  LidarCaptureApp.swift
//  LidarCapture
//
//  Created by Roberto Marani on 16/01/23.
//

import SwiftUI

@main
struct LidarCaptureApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
