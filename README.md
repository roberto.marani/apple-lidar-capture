# Apple Lidar Capture

App for Lidar Capture and storage in local File folder

## Installation
The app requires IOS 16.2 and a device with Lidar sensor (Ipad Pro or IPhone Pro).

Tested on Ipad Pro 2022 M2 (11")

## Usage
Clone the repository, build it (check the bundle) on your device.

## Support
For any contact, please send a mail to [roberto.marani@stiima.cnr.it](mailto:roberto.marani@stiima.cnr.it)

## License
The Software is released AS IS under an MIT license.

Copyright Roberto Marani - 2023

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
